<?php

namespace Database\Seeders;

use App\Models\Municipio;
use App\Models\RegionEducativa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MunicipioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $region = RegionEducativa::where('nombre', 'Guaicaipuro – Carrizal – Los Salias')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Guaicaipuro',
                ],
                [
                    'municio' => 'Carrizal'
                ],
                [
                    'municio' => 'Los Salias'
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Acevedo')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Acevedo',
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Andrés Bello - Páez')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Andrés Bello',
                ],
                [
                    'municio' => 'Páez'
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Brión - Buróz')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Brión',
                ],
                [
                    'municio' => 'Buroz'
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Pedro Gual')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Pedro Gual',
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Sucre')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Sucre',
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Chacao – Baruta – El Hatillo')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Baruta',
                ],
                [
                    'municio' => 'Chacao',
                ],
                [
                    'municio' => 'El Hatillo',
                ]
            ]);


        $region = RegionEducativa::where('nombre', 'Plaza - Zamora')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Plaza',
                ],
                [
                    'municio' => 'Zamora',
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Cristóbal Rojas - Urdaneta')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Cristóbal Rojas',
                ],
                [
                    'municio' => 'Urdaneta',
                ]
            ]);

        $region = RegionEducativa::where('nombre', 'Independencia - Paz Castillo')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Independencia',
                ],
                [
                    'municio' => 'Paz Castillo',
                ]
            ]);


        $region = RegionEducativa::where('nombre', 'Simón Bolívar - Tomás Lander')->first();
        Municipio::factory()
            ->state([
                'region_educativa_id' => $region->id
            ])
            ->createMany([
                [
                    'municio' => 'Simón Bolívar',
                ],
                [
                    'municio' => 'Tomás Lander',
                ]
            ]);
    }
}
