<?php

namespace Database\Seeders;

use App\Models\EjeTerritorial;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class EjeTerritorialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EjeTerritorial::factory()
            ->count(5)
            ->state(new Sequence(
                ['nombre' => 'Altos Mirandinos'],
                ['nombre' => 'Barlovento'],
                ['nombre' => 'Metropolitano'],
                ['nombre' => 'Plaza - Zamora'],
                ['nombre' => 'Valles del Tuy']
            ))->create();
    }
}
