<?php

namespace Database\Seeders;

use App\Models\EjeTerritorial;
use App\Models\RegionEducativa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class RegionEducativaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $altosMirandinos = EjeTerritorial::where('nombre', 'Altos Mirandinos')->first();
        RegionEducativa::factory()
            ->state(new Sequence([
                'eje_territorial_id' => $altosMirandinos->id
            ]))
            ->createMany([
                [
                    'nombre' => 'Guaicaipuro – Carrizal – Los Salias'
                ]
            ]);


        $Barlovento = EjeTerritorial::where('nombre', 'Barlovento')->first();
        RegionEducativa::factory()
            ->state(new Sequence([
                'eje_territorial_id' => $Barlovento->id
            ]))
            ->createMany([
                [
                    'nombre' => 'Acevedo'
                ],
                [
                    'nombre' => 'Andrés Bello - Páez'
                ],
                [
                    'nombre' => 'Brión - Buróz'
                ],
                [
                    'nombre' => 'Pedro Gual'
                ]
            ]);

        $Metropolitano = EjeTerritorial::where('nombre', 'Metropolitano')->first();
        RegionEducativa::factory()
            ->state(new Sequence([
                'eje_territorial_id' => $Metropolitano->id
            ]))
            ->createMany([
                [
                    'nombre' => 'Sucre'
                ],
                [
                    'nombre' => 'Chacao – Baruta – El Hatillo'
                ]
            ]);

        $plazazamora = EjeTerritorial::where('nombre', 'Plaza - Zamora')->first();
        RegionEducativa::factory()
            ->state(new Sequence([
                'eje_territorial_id' => $plazazamora->id
            ]))
            ->createMany([
                [
                    'nombre' => 'Plaza - Zamora'
                ]
            ]);

        $tuy = EjeTerritorial::where('nombre', 'Valles del Tuy')->first();
        RegionEducativa::factory()
            ->state(new Sequence([
                'eje_territorial_id' => $tuy->id
            ]))
            ->createMany([
                [
                    'nombre' => 'Cristóbal Rojas - Urdaneta'
                ],
                [
                    'nombre' => 'Independencia - Paz Castillo'
                ],
                [
                    'nombre' => 'Simón Bolívar - Tomás Lander'
                ]
            ]);
    }
}
