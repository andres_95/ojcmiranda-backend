<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'username' => 'admin',
            'role' => 'admin',
            'nombres' => 'admin',
            'apellidos' => 'ocj',
        ]);

        $this->call(
            [
                EstadosSeeder::class,
                EjeTerritorialSeeder::class,
                RegionEducativaSeeder::class,
                MunicipioSeeder::class
            ]
        );
    }
}
