<?php

namespace Database\Factories;

use App\Models\Estados;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Municipio>
 */
class MunicipioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $miranda = Estados::where('estado', 'Estado Miranda')->first();
        return [
            //
            'estados_id' => $miranda ? $miranda->id : 1
        ];
    }
}
