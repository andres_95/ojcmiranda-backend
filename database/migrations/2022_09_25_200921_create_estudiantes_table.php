<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\UnidadEducativa::class);
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('email');
            $table->string('cedula');
            $table->string('telefono');
            $table->string('anio_curso')->nullable();
            $table->enum('sexo',['F','M']);
            $table->date('nacimiento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudiantes');
    }
};
