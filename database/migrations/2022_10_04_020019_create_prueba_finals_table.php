<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prueba_finals', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Jornada::class);
            $table->foreignIdFor(\App\Models\UnidadEducativa::class);
            $table->integer('nota_teorica');
            $table->integer('nota_experimental');
            $table->boolean('obtuvo_medalla')->default(false);
            $table->enum('medalla', \App\Models\PruebaFinal::MEDALLAS)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prueba_finals');
    }
};
