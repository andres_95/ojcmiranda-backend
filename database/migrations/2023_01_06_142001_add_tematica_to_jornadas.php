<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('jornadas', function (Blueprint $table) {
            $table->string('tematica')->nullable()->after('lugar');
        });
    }

    public function down()
    {
        Schema::table('jornadas', function (Blueprint $table) {
            //
        });
    }
};
