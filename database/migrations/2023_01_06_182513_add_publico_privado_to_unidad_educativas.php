<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('unidad_educativas', function (Blueprint $table) {
            $table->boolean('publica')->default(false);
            $table->boolean('privada')->default(false);
        });
    }

    public function down()
    {
        Schema::table('unidad_educativas', function (Blueprint $table) {
            //
        });
    }
};
