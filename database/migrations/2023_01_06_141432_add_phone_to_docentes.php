<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('docentes', function (Blueprint $table) {
            $table->string('telefono')->nullable()->after('asignatura_adicional');
        });
    }

    public function down()
    {
        Schema::table('docentes', function (Blueprint $table) {
            //
        });
    }
};
