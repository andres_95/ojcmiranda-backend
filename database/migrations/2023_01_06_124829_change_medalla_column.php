<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('prueba_finals', function (Blueprint $table) {
            $table->string('medalla')->nullable();
        });
    }

    public function down()
    {
        Schema::table('prueba_finals', function (Blueprint $table) {
            //
        });
    }
};
