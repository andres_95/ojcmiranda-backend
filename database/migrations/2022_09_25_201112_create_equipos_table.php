<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\UnidadEducativa::class);
            $table->foreignIdFor(\App\Models\Jornada::class);
            $table->foreignIdFor(\App\Models\Estudiante::class,'estudiante1');
            $table->foreignIdFor(\App\Models\Estudiante::class,'estudiante2');
            $table->foreignIdFor(\App\Models\Estudiante::class,'estudiante3');
            $table->foreignIdFor(\App\Models\Estudiante::class,'estudiante4');
            $table->foreignIdFor(\App\Models\Estudiante::class,'estudiante5');
            $table->boolean('estatus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
};
