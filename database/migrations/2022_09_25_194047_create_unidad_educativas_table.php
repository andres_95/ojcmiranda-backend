<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidad_educativas', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\EjeTerritorial::class);
            $table->foreignIdFor(\App\Models\RegionEducativa::class);
            $table->foreignIdFor(\App\Models\Municipio::class);
            $table->string('nombre');
            $table->boolean('estatus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidad_educativas');
    }
};
