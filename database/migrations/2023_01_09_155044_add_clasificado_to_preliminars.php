<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::table('preliminars', function (Blueprint $table) {
            $table->boolean('clasificado')->default(false);
        });
    }

    public function down()
    {
        Schema::table('preliminars', function (Blueprint $table) {
            //
        });
    }
};
