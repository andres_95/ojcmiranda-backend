<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EquipoController;
use App\Http\Controllers\EstadosController;
use App\Http\Controllers\JornadaController;
use App\Http\Controllers\PreliminarController;
use App\Http\Controllers\PreRegistroController;
use App\Http\Controllers\PruebaFinalController;
use App\Http\Controllers\UnidadEducativaController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'auth', 'name' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('/registro', [AuthController::class, 'registroSchool'])->name('auth.registro-school');
    Route::post('preregistro', [PreRegistroController::class, 'store'])->name('pre-registro.store');
    Route::group(['prefix' => 'password'], function () {
        Route::post('/recovery', [AuthController::class, 'PasswordRecovery'])->name('password-recovery');
        Route::post('/change', [AuthController::class, 'PasswordChange'])->name('auth.change-password');
    });
});


Route::group(['middleware' => 'auth:sanctum'], function () {

    Route::group(['prefix' => 'account', 'name' => 'account'], function () {
        Route::get('/', [AccountController::class, 'account'])->name('account.account');
    });

    /**
     * Rutas para administrador
     *
     *
     */
    Route::group(['middleware' => 'ability:admin'], function () {

        Route::apiResource('pre-registro', PreRegistroController::class);
        Route::post('pre-registro/{preRegistro}/autorizar', [PreRegistroController::class, 'autorizar'])
            ->name('pre-registro.autorizar');
        Route::get('/jornada/equipos', [JornadaController::class,'equipos'])->name('jornada.equipos');
        Route::get('/jornada/{jornada}/equipo', [JornadaController::class,'equipo'])->name('jornada.equipo');
        Route::get('/jornada/{jornada}/preliminar/export', [JornadaController::class,'exportPreliminar'])->name('export-preliminar');
        Route::get('/jornada/{jornada}/finales/export', [JornadaController::class,'exportFinales'])->name('export-finales');


        Route::get('/jornada/preliminares', [JornadaController::class,'preliminares'])->name('jornada.preliminares');

        Route::get('/jornada/{jornada}/preliminar', [JornadaController::class,'preliminar'])->name('jornada.preliminar');
        Route::apiResource('jornada', JornadaController::class)->except('index');





        Route::apiResource('equipo', EquipoController::class)->except(['store']);



        Route::get('/equipo/{equipo}/activate',[EquipoController::class,'activate']);

        Route::get('/unidad_educativa/{unidadEducativa}/activar', [UnidadEducativaController::class, 'activar']);

        Route::apiResource('usuario', UserController::class)->only(['store']);

        Route::apiResource('preliminar', PreliminarController::class);

        Route::apiResource('prueba_final', PruebaFinalController::class);
    });

    /**
     * Rutas para school
     *
     *
     */
    Route::group(['middleware' => 'ability:school'], function (){
        Route::apiResource('equipo', EquipoController::class)->only('store');
        Route::apiResource('unidad_educativa', UnidadEducativaController::class)->only(['store']);
        Route::get('/unidad_educativa/{unidadEducativa}/estatus', [UnidadEducativaController::class, 'estatus']);
    });



    Route::apiResource('jornada', JornadaController::class)->only('index');

    Route::apiResource('estado', EstadosController::class);


    Route::apiResource('unidad_educativa', UnidadEducativaController::class)->only(['show', 'index','update','destroy']);








});
