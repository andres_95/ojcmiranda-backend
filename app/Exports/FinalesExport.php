<?php

namespace App\Exports;

use App\Models\Jornada;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class FinalesExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithCustomStartCell,WithTitle
{

    public function __construct(Jornada $jornada)
    {
        $this->jornada = $jornada;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
        return $this->jornada->pruebas_finales()
            ->with('unidad_educativa.municipio.regionEducativa')
            ->with('equipoR')
            ->orderBy('obtuvo_medalla','desc')
            ->orderBy('medalla')
            ->get();
    }

    public function headings(): array
    {
        return [
            'Unidad Educativa',
            'Nota Teorica',
            'Nota Experimental',
            'Medalla',
            'Lugar',
            'Municipio',
            'Region Educativa',
            'Docente',
            'Estudiante 1',
            'Grado',
            'Estudiante 2',
            'Grado',
            'Estudainte 3',
            'Grado',
            'Estudiante 4',
            'Grado',
            'Estudiante 5',
            'Grado'
        ];
    }

    public function map($row): array
    {
        return [
            $row->unidad_educativa->nombre,
            $row->nota_teorica,
            $row->nota_experimental,
            $row->obtuvo_medalla ? 'SI' : 'NO',
            $row->medalla,
            $row->unidad_educativa->municipio->municio,
            $row->unidad_educativa->municipio->regionEducativa->nombre,
            $row->equipoR->docente->fullname,
            $row->equipoR->estudiante_1->fullname,
            $row->equipoR->estudiante_1->anio_curso.' grado',
            $row->equipoR->estudiante_2->fullname,
            $row->equipoR->estudiante_2->anio_curso.' grado',
            $row->equipoR->estudiante_3->fullname,
            $row->equipoR->estudiante_3->anio_curso.' grado',
            $row->equipoR->estudiante_4->fullname,
            $row->equipoR->estudiante_4->anio_curso.' grado',
            $row->equipoR->estudiante_5->fullname,
            $row->equipoR->estudiante_5->anio_curso.' grado',
        ];
    }

    public function startCell(): string
    {
        return 'B2';
    }


    public function title(): string
    {
        return 'Resultados Finales';
    }
}
