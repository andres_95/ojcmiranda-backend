<?php

namespace App\Exports;

use App\Models\Jornada;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class PreliminarExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithCustomStartCell,WithTitle
{
    private $jornada;

    public function __construct(Jornada $jornada)
    {
        $this->jornada = $jornada;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        //
        return $this->jornada->preliminares()
            ->with('unidad_educativa.municipio.regionEducativa')
            ->with('equipoR')
            ->orderBy('nota', 'desc')
            ->orderBy('clasificado', 'desc')
            ->get();
    }

    public function headings(): array
    {
        return [
            'Unidad Educativa',
            'Nota',
            'Clasificado',
            'Municipio',
            'Region Educativa',
            'Docente',
            'Estudiante 1',
            'Grado',
            'Estudiante 2',
            'Grado',
            'Estudainte 3',
            'Grado',
            'Estudiante 4',
            'Grado',
            'Estudiante 5',
            'Grado'
        ];
    }

    public function map($row): array
    {
        return [
            $row->unidad_educativa->nombre,
            $row->nota,
            $row->clasificado ? 'SI' : 'NO',
            $row->unidad_educativa->municipio->municio,
            $row->unidad_educativa->municipio->regionEducativa->nombre,
            $row->equipoR->docente->fullname,
            $row->equipoR->estudiante_1->fullname,
            $row->equipoR->estudiante_1->anio_curso.' grado',
            $row->equipoR->estudiante_2->fullname,
            $row->equipoR->estudiante_2->anio_curso.' grado',
            $row->equipoR->estudiante_3->fullname,
            $row->equipoR->estudiante_3->anio_curso.' grado',
            $row->equipoR->estudiante_4->fullname,
            $row->equipoR->estudiante_4->anio_curso.' grado',
            $row->equipoR->estudiante_5->fullname,
            $row->equipoR->estudiante_5->anio_curso.' grado',
        ];
    }

    public function startCell(): string
    {
        return 'B2';
    }


    public function title(): string
    {
        return 'Resultados de indagación';
    }
}
