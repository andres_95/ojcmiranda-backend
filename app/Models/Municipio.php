<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Municipio extends Model
{
    use HasFactory;

    protected $fillable = [
        'municio'
    ];

    protected $hidden = [
        'updated_at', 'created_at'
    ];

    public function regionEducativa(): BelongsTo
    {
        return $this->belongsTo(RegionEducativa::class, 'region_educativa_id');
    }
}
