<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegionEducativa extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $with = ['municipios'];

    public function municipios()
    {
        return $this->hasMany(Municipio::class);
    }
}
