<?php

namespace App\Models;

use App\Models\Scopes\useGetOrPaginate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    use HasFactory, useGetOrPaginate;

    protected $fillable = [
        'estado',
        'iso_3166-2'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    protected $with = ['ejes_territoriales', 'municipios'];

    public function ejes_territoriales()
    {
        return $this->hasMany(EjeTerritorial::class);
    }

    public function municipios()
    {
        return $this->hasMany(Municipio::class);
    }
}
