<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombres',
        'apellidos',
        'email',
        'cedula',
        'telefono',
        'sexo',
        'nacimiento',
        'anio_curso',
        'unidad_educativa_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getFullnameAttribute(): string
    {
        return $this->nombres . ' '. $this->apellidos;
    }
}
