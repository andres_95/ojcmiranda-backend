<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class filterFillableScope implements Scope
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function apply(Builder $builder, Model $model)
    {
        $filter = request()->get('filter', '');
        $value = request()->get('value', '');
        if ($model->isFillable($filter)) {
            $builder->where($filter, $value);
        }

        $filter2 = request()->get('filter2', '');
        $value2 = request()->get('value2', '');
        if ($model->isFillable($filter2)) {
            $builder->where($filter2, $value2);
        }
    }
}
