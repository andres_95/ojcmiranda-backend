<?php

namespace App\Models\Scopes;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

trait useGetOrPaginate
{
    public function scopeGetOrPaginate(Builder $query): Collection|LengthAwarePaginator|array
    {
        if (request()->get('perPage')) {
            $perPage = intval(request('perPage'));

            if ($perPage) {
                return $query->paginate($perPage);
            }
        }

        return $query->get();
    }
}
