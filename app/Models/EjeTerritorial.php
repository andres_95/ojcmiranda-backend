<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EjeTerritorial extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre'
    ];

    protected $hidden = [
        'updated_at', 'created_at'
    ];

    protected $with = ['regiones_educativas'];

    public function regiones_educativas()
    {
        return $this->hasMany(RegionEducativa::class);
    }
}
