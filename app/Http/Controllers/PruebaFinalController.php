<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePruebaFinalRequest;
use App\Http\Requests\UpdatePruebaFinalRequest;
use App\Models\Preliminar;
use App\Models\PruebaFinal;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Validation\ValidationException;

class PruebaFinalController extends Controller
{
    public function index()
    {

        $data = PruebaFinal::with(['jornada', 'unidad_educativa.municipio'])
            ->paginate();

        return JsonResource::collection($data);
    }


    public function store(StorePruebaFinalRequest $request)
    {
        //
        $data = $request->validated();
        $check = PruebaFinal::where('jornada_id', $data['jornada_id'])
            ->where('unidad_educativa_id', $data['unidad_educativa_id'])
            ->first();

        if ($check) {
            throw ValidationException::withMessages([
                'Duplicate' => ['Ya fue registrada una prueba final para este colegio en la jornada seleccionada.'],
            ]);
        }

        $preliminar = Preliminar::where('jornada_id', $data['jornada_id'])
            ->where('unidad_educativa_id', $data['unidad_educativa_id'])
            ->first();

        if (!$preliminar) {
            throw ValidationException::withMessages([
                'no fount' => ['El instituto no posee prueba preliminar en esta edición'],
            ]);
        }

        if (!$preliminar->clasificado)
            throw ValidationException::withMessages([
                'no fount' => ['El instituto no esta clasificado en esta edición'],
            ]);

        $prueba_final = PruebaFinal::create($data);

        $prueba_final->load('jornada');

        $prueba_final->load(['unidad_educativa.equipos' => function ($query) use ($prueba_final) {
            $query->where('jornada_id', $prueba_final->jornada->id)->first();
        }, 'unidad_educativa.municipio']);

        return new JsonResource($prueba_final->append('equipo'));
    }


    public function show(PruebaFinal $pruebaFinal)
    {
        $pruebaFinal->load(['unidad_educativa.municipio', 'jornada']);
        return $pruebaFinal->append('equipo');
    }


    public function update(UpdatePruebaFinalRequest $request, PruebaFinal $pruebaFinal)
    {
        $data = $request->validated();
        $check = PruebaFinal::where('jornada_id', $data['jornada_id'])
            ->where('unidad_educativa_id', $data['unidad_educativa_id'])
            ->where('id', '!=', $pruebaFinal->id)
            ->first();

        if ($check) {
            throw ValidationException::withMessages([
                'Duplicate' => ['Ya fue registrada una prueba final para este colegio en la jornada seleccionada.'],
            ]);
        }

        $preliminar = Preliminar::where('jornada_id', $data['jornada_id'])
            ->where('unidad_educativa_id', $data['unidad_educativa_id'])
            ->first();

        if (!$preliminar) {
            throw ValidationException::withMessages([
                'no fount' => ['El instituto no posee prueba preliminar en esta edición'],
            ]);
        }

        $pruebaFinal->update($data);
        $pruebaFinal->save();

        $pruebaFinal->load('jornada');

        $pruebaFinal->load(['unidad_educativa.equipos' => function ($query) use ($pruebaFinal) {
            $query->where('jornada_id', $pruebaFinal->jornada->id)->first();
        }, 'unidad_educativa.municipio']);

        return new JsonResource($pruebaFinal->append('equipo'));
    }

    public function destroy(PruebaFinal $pruebaFinal)
    {
        //
    }
}
