<?php

namespace App\Http\Controllers;

use App\Exports\FinalesExport;
use App\Exports\PreliminarExport;
use App\Http\Requests\StoreJornadaRequest;
use App\Http\Requests\UpdateJornadaRequest;
use App\Models\Jornada;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Resources\Json\JsonResource;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Exception;

class JornadaController extends Controller
{
    public function index()
    {
        return JsonResource::collection(Jornada::getOrPaginate());
    }


    public function store(StoreJornadaRequest $request)
    {
        $data = $request->validated();

        return Jornada::create($data);
    }

    public function show(Jornada $jornada)
    {
        return new JsonResource($jornada);
    }

    public function update(UpdateJornadaRequest $request, Jornada $jornada)
    {
        $data = $request->validated();
        $jornada->update($data);
        $jornada->save();
        return new JsonResource($jornada);
    }


    public function destroy(Jornada $jornada)
    {
        //
    }

    public function equipos()
    {
        $jornadas = Jornada::with(['equipos' => function ($query) {
            $query->where('estatus', 1)
                ->without([
                    'jornada',
                    'estudiante_1',
                    'estudiante_2',
                    'estudiante_3',
                    'estudiante_4',
                    'estudiante_5',
                    'docente'
                ]);
        }])->getOrPaginate();

        return new JsonResource($jornadas);
    }

    public function equipo(Jornada $jornada)
    {
        $jornada->load(['equipos' => function ($query) {
            $query->where('estatus', 1)
                ->without([
                    'jornada',
                    'estudiante_1',
                    'estudiante_2',
                    'estudiante_3',
                    'estudiante_4',
                    'estudiante_5',
                    'docente'
                ]);
        }]);

        return new JsonResource($jornada);
    }

    public function preliminares()
    {
        $jornadas = Jornada::with(['preliminares' => function (HasMany $query) {
            $query->where('clasificado', 1)
                ->with('unidad_educativa')
                ->withoutGlobalScopes();
        }])->getOrPaginate();

        return new JsonResource($jornadas);
    }

    public function preliminar(Jornada $jornada)
    {
        $jornada->load(['preliminares' => function (HasMany $query) {
            $query->where('clasificado', 1)
                ->with('unidad_educativa');
        }]);

        return new JsonResource($jornada);
    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportPreliminar(Jornada $jornada)
    {
        return Excel::download(
            new PreliminarExport($jornada),
            'Jornada ' . $jornada->anio_inicio . ' ' . $jornada->anio_fin . '.xlsx',
            \Maatwebsite\Excel\Excel::XLSX);
    }

    public function exportFinales(Jornada $jornada) {
        return Excel::download(
            new FinalesExport($jornada),
            'Jornada ' . $jornada->anio_inicio . ' ' . $jornada->anio_fin . '.xlsx',
            \Maatwebsite\Excel\Excel::XLSX);
    }
}
