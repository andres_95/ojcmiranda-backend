<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEstudianteRequest;
use App\Http\Requests\UpdateEstudianteRequest;
use App\Models\Estudiante;
use Illuminate\Http\Response;

class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEstudianteRequest $request
     * @return Response
     */
    public function store(StoreEstudianteRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Estudiante $estudiante
     * @return Response
     */
    public function show(Estudiante $estudiante)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEstudianteRequest $request
     * @param Estudiante $estudiante
     * @return Response
     */
    public function update(UpdateEstudianteRequest $request, Estudiante $estudiante)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Estudiante $estudiante
     * @return Response
     */
    public function destroy(Estudiante $estudiante)
    {
        //
    }
}
