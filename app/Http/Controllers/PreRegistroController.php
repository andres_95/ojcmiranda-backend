<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePreRegistroRequest;
use App\Http\Requests\UpdatePreRegistroRequest;
use App\Models\PreRegistro;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class PreRegistroController extends Controller
{

    public function index()
    {
        //
        $data = PreRegistro::paginate();

        return JsonResource::collection($data);
    }


    public function store(StorePreRegistroRequest $request)
    {
        //
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        return PreRegistro::create($data);
    }


    public function show(PreRegistro $preRegistro)
    {
        //
        return $preRegistro;
    }


    public function update(UpdatePreRegistroRequest $request, PreRegistro $preRegistro)
    {
        //
    }


    public function destroy(PreRegistro $preRegistro)
    {
        //
    }

    public function autorizar(PreRegistro $preRegistro)
    {
        if ($preRegistro->estatus) {
            throw ValidationException::withMessages([
                'estatus' => ['Este pre registro ya esta activo.'],
            ]);
        }

        $checkUser = User::where('username', $preRegistro->username)->first();

        if ($checkUser) {
            throw ValidationException::withMessages([
                'user' => ['Este pre registro ya tiene un usuario.'],
            ]);
        }

        $user = User::create([
            'username' => $preRegistro->username,
            'email' => $preRegistro->email,
            'password' => $preRegistro->password,
            'role' => 'school'
        ]);

        $preRegistro->estatus = 1;

        $preRegistro->save();

        return response()->json(compact('user', 'preRegistro'), 201);
    }
}
