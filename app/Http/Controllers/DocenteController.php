<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDocenteRequest;
use App\Http\Requests\UpdateDocenteRequest;
use App\Models\Docente;
use Illuminate\Http\Response;

class DocenteController extends Controller
{

    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDocenteRequest $request
     * @return Response
     */
    public function store(StoreDocenteRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Docente $docente
     * @return Response
     */
    public function show(Docente $docente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDocenteRequest $request
     * @param Docente $docente
     * @return Response
     */
    public function update(UpdateDocenteRequest $request, Docente $docente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Docente $docente
     * @return Response
     */
    public function destroy(Docente $docente)
    {
        //
    }
}
