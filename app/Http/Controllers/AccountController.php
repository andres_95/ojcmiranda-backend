<?php

namespace App\Http\Controllers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    //
    public function account()
    {
        $user = Auth::user();
        if ($user->role === 'school') {
            $user->load('unidad_educativa');
        }
        return new JsonResource($user);
    }
}
