<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRegionEducativaRequest;
use App\Http\Requests\UpdateRegionEducativaRequest;
use App\Models\RegionEducativa;

class RegionEducativaController extends Controller
{

    public function index()
    {
        //
    }


    public function store(StoreRegionEducativaRequest $request)
    {
        //
    }


    public function show(RegionEducativa $regionEducativa)
    {
        //
    }


    public function update(UpdateRegionEducativaRequest $request, RegionEducativa $regionEducativa)
    {
        //
    }


    public function destroy(RegionEducativa $regionEducativa)
    {
        //
    }
}
