<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePreliminarRequest;
use App\Http\Requests\UpdatePreliminarRequest;
use App\Models\Preliminar;
use App\Models\UnidadEducativa;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Validation\ValidationException;

class PreliminarController extends Controller
{
    public function index()
    {
        $data = Preliminar::with(['unidad_educativa.municipio', 'jornada'])->paginate();

        return JsonResource::collection($data);
    }


    public function store(StorePreliminarRequest $request)
    {
        $data = $request->validated();
        $check = Preliminar::where('jornada_id', $data['jornada_id'])
            ->where('unidad_educativa_id', $data['unidad_educativa_id'])
            ->first();

        if ($check) {
            throw ValidationException::withMessages([
                'Duplicate' => ['Ya fue registrada una preliminar para este colegio en la jornada seleccionada.'],
            ]);
        }

        $insituto = UnidadEducativa::findOrFail($data['unidad_educativa_id']);

        $insituto->load(['equipos' => function ($query) use ($data) {
            $query->where('jornada_id', $data['jornada_id']);
        }]);

        if (!$insituto->equipos->count())
            throw ValidationException::withMessages([
                'no fount' => ['El insituto ' . $insituto->nombre . ' no tiene un equipo registrado en esta edición'],
            ]);

        $preliminar = Preliminar::create($data);

        return new JsonResource($preliminar->append('equipo'));
    }

    public function show(Preliminar $preliminar)
    {
        //
        $preliminar->load('jornada');
        $preliminar->load(['unidad_educativa.municipio']);
        return response($preliminar->append('equipo'));
    }


    public function update(UpdatePreliminarRequest $request, Preliminar $preliminar)
    {
        $data = $request->validated();


        $insituto = UnidadEducativa::findOrFail($data['unidad_educativa_id']);

        $insituto->load(['equipos' => function ($query) use ($data) {
            $query->where('jornada_id', $data['jornada_id']);
        }]);

        if (!$insituto->equipos->count())
            throw ValidationException::withMessages([
                'no fount' => ['El insituto ' . $insituto->nombre . ' no tiene un equipo registrado en esta edición'],
            ]);

        $preliminar->update($data);
        $preliminar->save();

        return new JsonResource($preliminar->append('equipo'));
    }

    public function destroy(Preliminar $preliminar)
    {
        //
        return $preliminar->delete();
    }
}
