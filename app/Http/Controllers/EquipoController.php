<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEquipoRequest;
use App\Http\Requests\UpdateEquipoRequest;
use App\Models\Equipo;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class EquipoController extends Controller
{

    public function index()
    {
        return JsonResource::collection(Equipo::paginate());
    }


    public function store(StoreEquipoRequest $request)
    {
        $user = Auth::user();

        $data = $request->validated();

        if (!$user) throw ValidationException::withMessages([
            'user' => ['Usuario invalido para realizar esta acción.'],
        ]);

        $unidad_educativa = $user->unidad_educativa;

        if (!$unidad_educativa) {
            throw ValidationException::withMessages([
                'user' => ['Debe registrar la unidad educativa.'],
            ]);
        }

        if (!$unidad_educativa->estatus) {
            throw ValidationException::withMessages([
                'unidad_educativa' => ['El registro de unidad educativa no se encuentra aprobado.'],
            ]);
        }

        $tercero = false;
        $cuarto = false;
        $quinto = false;

        foreach ($data['estudiante'] as $clave => $item) {
            if (intval($item['anio_curso']) === 3) $tercero = true;
            if (intval($item['anio_curso']) === 4) $cuarto = true;
            if (intval($item['anio_curso']) === 5) $quinto = true;
            foreach ($data['estudiante'] as $clave2 => $item2)  {
                if ($item['cedula'] === $item2['cedula'] && $clave2 !== $clave)
                    throw ValidationException::withMessages([
                        'equipo' => ['No pueden haber cedulas repetidas en el equipo'],
                    ]);
            }
        }

        if (!$tercero || !$cuarto || !$quinto) {
            throw ValidationException::withMessages([
                'equipo' => ['Debe haber al menos un estudiante para cada año de curso (3ro, 4to, 5to).'],
            ]);
        }

        $docente = $unidad_educativa->docentes()->create([
            'email' => $data['email_docente'],
            'nombres' => $data['nombre_docente'],
            'apellidos' => $data['apellido_docente'],
            'asignatura' => $data['asignatura_docente'],
            'asignatura_adicional' => $data['asignatura_adicional_docente'],
            'telefono' => array_key_exists('telefono_docente', $data)? $data['telefono_docente']: ''
        ]);

        $estudiantes = collect([]);

        foreach ($data['estudiante'] as $item) {
            $estudiante = $unidad_educativa->estudiantes()->create($item);
            $estudiantes->push($estudiante);
        }

        $equipo = $unidad_educativa->equipos()->create([
            'jornada_id' => $data['jornada'],
            'docente_id' => $docente->id,
            'estudiante1' => $estudiantes[0]->id,
            'estudiante2' => $estudiantes[1]->id,
            'estudiante3' => $estudiantes[2]->id,
            'estudiante4' => $estudiantes[3]->id,
            'estudiante5' => $estudiantes[4]->id,
        ]);

        $equipo->load(['jornada', 'unidad_educativa', 'docente', 'estudiante_1', 'estudiante_2', 'estudiante_3', 'estudiante_4', 'estudiante_5']);

        return $equipo;
    }


    public function show(Equipo $equipo)
    {
        $equipo->load(['jornada', 'unidad_educativa', 'docente', 'estudiante_1', 'estudiante_2', 'estudiante_3', 'estudiante_4', 'estudiante_5']);

        return response($equipo);
    }


    public function update(UpdateEquipoRequest $request, Equipo $equipo)
    {
        //
    }


    public function destroy(Equipo $equipo)
    {
        //
    }

    public function activate(Equipo $equipo)
    {
        if ($equipo->estatus) {
            throw ValidationException::withMessages([
                'estatus' => ['Este Equipo ya esta activo.'],
            ]);
        }

        $equipo->estatus = 1;
        $equipo->save();

        return $equipo;
    }

}
