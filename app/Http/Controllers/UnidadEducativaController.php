<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUnidadEducativaRequest;
use App\Http\Requests\UpdateUnidadEducativaRequest;
use App\Models\UnidadEducativa;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UnidadEducativaController extends Controller
{
    public function index()
    {
        return JsonResource::collection(UnidadEducativa::getOrPaginate());
    }

    public function store(StoreUnidadEducativaRequest $request)
    {
        $user = Auth::user();

        if ($user->role !== 'school') {
            throw ValidationException::withMessages([
                'user' => ['Usuario invalido para realizar esta acción.'],
            ]);
        }

        if ($user->unidad_educativa)
            throw ValidationException::withMessages([
                'user' => ['Ya posee una unidad educativa registrada.'],
            ]);


        $data = $request->validated();

        $unidad_educativa = UnidadEducativa::create([
            'nombre' => $data['nombre'],
            'eje_territorial_id' => $data['eje_territorial'],
            'region_educativa_id' => $data['region_educativa'],
            'municipio_id' => $data['municipio'],
            'tipo' => $data['tipo'],
            'publica' => $data['publica']?: 0,
            'privada' => $data['privada']?: 0
        ]);

        $user->unidad_educativa_id = $unidad_educativa->id;

        $user->save();

        return new JsonResource($unidad_educativa);
    }

    public function show(UnidadEducativa $unidadEducativa)
    {
        //
        $user = Auth::user();

        if ($user->role === 'school' && $user->unidad_educativa_id !== $unidadEducativa->id) {
            throw ValidationException::withMessages([
                'user' => 'Usted solo puede ver el detalle de su propia unidad educativa.'
            ]);
        }

        return new JsonResource($unidadEducativa);
    }

    public function update(UpdateUnidadEducativaRequest $request, UnidadEducativa $unidadEducativa)
    {
        $data = $request->validated();
        $unidadEducativa->update($data);
        $unidadEducativa->municipio_id = $data['municipio'];
        $unidadEducativa->save();
        return new JsonResource($unidadEducativa);
    }

    public function destroy(UnidadEducativa $unidadEducativa)
    {
        //
        $unidadEducativa->loadCount(['docentes', 'equipos', 'estudiantes', 'pruebasFinales', 'preliminares']);
        if ($unidadEducativa->docentes_count || $unidadEducativa->equipos_count
            || $unidadEducativa->estudiantes_count || $unidadEducativa->pruebas_finales_count
            || $unidadEducativa->preliminares_count
            || $unidadEducativa->estatus
        )
            throw ValidationException::withMessages([
                'user' => 'Esta unidad educativa ya posee data ingresada no puede ser eliminada.'
            ]);

        return $unidadEducativa->delete();
    }

    public function estatus(UnidadEducativa $unidadEducativa)
    {
        $unidadEducativa->load(['equipos', 'pruebasFinales.jornada']);
        return new JsonResource($unidadEducativa);
    }

    public function activar(UnidadEducativa $unidadEducativa)
    {
        if ($unidadEducativa->estatus) {
            throw ValidationException::withMessages([
                'estatus' => ['Esta Unidad Educativa ya esta activa.'],
            ]);
        }

        $unidadEducativa->estatus = 1;

        $unidadEducativa->save();

        return $unidadEducativa;
    }
}
