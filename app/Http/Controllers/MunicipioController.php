<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMunicipioRequest;
use App\Http\Requests\UpdateMunicipioRequest;
use App\Models\Municipio;
use Illuminate\Http\Response;

class MunicipioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMunicipioRequest $request
     * @return Response
     */
    public function store(StoreMunicipioRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Municipio $municipio
     * @return Response
     */
    public function show(Municipio $municipio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMunicipioRequest $request
     * @param Municipio $municipio
     * @return Response
     */
    public function update(UpdateMunicipioRequest $request, Municipio $municipio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Municipio $municipio
     * @return Response
     */
    public function destroy(Municipio $municipio)
    {
        //
    }
}
