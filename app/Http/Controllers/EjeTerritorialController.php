<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEjeTerritorialRequest;
use App\Http\Requests\UpdateEjeTerritorialRequest;
use App\Models\EjeTerritorial;
use Illuminate\Http\Response;

class EjeTerritorialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEjeTerritorialRequest $request
     * @return Response
     */
    public function store(StoreEjeTerritorialRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param EjeTerritorial $ejeTerritorial
     * @return Response
     */
    public function show(EjeTerritorial $ejeTerritorial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEjeTerritorialRequest $request
     * @param EjeTerritorial $ejeTerritorial
     * @return Response
     */
    public function update(UpdateEjeTerritorialRequest $request, EjeTerritorial $ejeTerritorial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param EjeTerritorial $ejeTerritorial
     * @return Response
     */
    public function destroy(EjeTerritorial $ejeTerritorial)
    {
        //
    }
}
