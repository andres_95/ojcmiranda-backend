<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        return response(User::paginate());
    }

    public function store(StoreUserRequest $request)
    {
        $data = $request->validated();
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'role' => 'admin',
            'password' => Hash::make($data['password']),
            'nombres' => $data['nombres'],
            'apellidos' => $data['apellidos']
        ]);
    }
}
