<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEstadosRequest;
use App\Http\Requests\UpdateEstadosRequest;
use App\Models\Estados;
use Illuminate\Http\Response;

class EstadosController extends Controller
{

    public function index()
    {
        return Estados::getOrPaginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEstadosRequest $request
     * @return Response
     */
    public function store(StoreEstadosRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Estados $estados
     * @return Response
     */
    public function show(Estados $estados)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEstadosRequest $request
     * @param Estados $estados
     * @return Response
     */
    public function update(UpdateEstadosRequest $request, Estados $estados)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Estados $estados
     * @return Response
     */
    public function destroy(Estados $estados)
    {
        //
    }
}
