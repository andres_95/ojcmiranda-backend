<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\PasswordChangeRequest;
use App\Http\Requests\PasswordRecoveryRequest;
use App\Http\Requests\RegistroSchoolRequest;
use App\Models\User;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $data = $request->validated();

        $user = User::where('username', $data['username'])->first();

        if (!$user || !Hash::check($data['password'], $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['Las credenciales son incorrectas.'],
            ]);
        }
        return [
            'user' => $user,
            'token' => $user->createToken($user->email, [$user->role])->plainTextToken
        ];
    }

    public function registroSchool(RegistroSchoolRequest $request)
    {
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => $data['password'],
            'role' => 'school',
            'nombres' => $data['unidad_educativa']
        ]);

        return response()->json($user, 201);
    }

    public function PasswordRecovery(PasswordRecoveryRequest $request)
    {
        $find = DB::table('password_resets')->where('email', $request->only('email'))->first();
        if ($find) {
            DB::table('password_resets')->where('email', $request->only('email'))->delete();
        }
        $token = Str::random(8);
        $code = DB::table('password_resets')
            ->insert([
                'email' => $request->get('email'),
                'token' => $token,
                'created_at' => now()
            ]);

        if ($code) {
            $user = User::query()->where('email', $request->get('email'))->first();

            $user->notify(
                (new ResetPasswordNotification($user, $token))
            );
            response()->json();

        } else {
            throw ValidationException::withMessages([
                'email' => ['Las credenciales son incorrectas.'],
            ]);
        }

    }

    public function PasswordChange(PasswordChangeRequest $request)
    {
        $find = DB::table('password_resets')
            ->where('email', $request->get('email'))
            ->where('token', $request->get('token'))
            ->first();
        if (!$find) {
            throw ValidationException::withMessages([
                'token' => ['Codigo de recuperación invalido.'],
            ]);
        }

        $user = User::where('email', $request->get('email'))->first();
        if (!$user) {
            throw ValidationException::withMessages([
                'email' => ['Email invalido.'],
            ]);
        }

        if (Carbon::parse($find->created_at)->addMinutes(180)->isPast()) {
            DB::table('password_resets')->where('email', $request->only('email'))->delete();
            throw ValidationException::withMessages([
                'token' => ['Codigo de recuperación invalido.'],
            ]);
        }

        $user->password = bcrypt($request->get('password'));
        $user->save();
        DB::table('password_resets')->where('email', $request->only('email'))->delete();
        event(new PasswordReset($user));
        return response()->json();

    }

}
