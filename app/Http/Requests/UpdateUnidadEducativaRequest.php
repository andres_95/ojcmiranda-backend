<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateUnidadEducativaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'eje_territorial' => 'required|numeric|exists:eje_territorials,id',
            'region_educativa' => 'required|numeric|exists:region_educativas,id',
            'municipio' => 'required|numeric|exists:municipios,id',
            'nombre' => [
                'required',
                'string',
                'max:255',
                Rule::unique('unidad_educativas', 'nombre')
                    ->ignore($this->route('unidad_educativa')->id)
            ],
            'publica' => 'sometimes|boolean',
            'privada' => 'sometimes|boolean',
            'tipo' => ['required','string',Rule::in(['Nacional','Estadal','Gestión Privada','Otro'])]
        ];
    }
}
