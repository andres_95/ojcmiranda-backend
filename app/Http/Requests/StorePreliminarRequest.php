<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StorePreliminarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        return $user && $user->tokenCan('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            'nota' => 'required|numeric',
            'jornada_id' => 'required|numeric|exists:jornadas,id',
            'unidad_educativa_id' => 'required|numeric|exists:unidad_educativas,id',
            'clasificado' => 'sometimes|boolean'
        ];
    }
}
