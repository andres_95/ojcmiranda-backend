<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreUserRequest extends FormRequest
{
    public function authorize(): bool
    {
        $user = Auth::user();
        return $user && $user->tokenCan('admin');
    }

    public function rules(): array
    {
        return [
            'email' => 'required|string|max:255|unique:users',
            'username' => 'required|string|max:255|unique:users',
            'password' => 'required|string|confirmed',
            'nombres' => 'required|string|max:255',
            'apellidos' => 'required|string|max:255'
        ];
    }
}
