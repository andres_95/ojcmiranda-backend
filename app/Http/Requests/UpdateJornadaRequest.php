<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateJornadaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        return $user && $user->tokenCan('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nombre' => [
                'required',
                'string',
                'max:255',
                Rule::unique('jornadas')
                    ->ignore($this->route('jornada'))
            ],
            'anio_inicio' => 'required|string|max:10',
            'anio_fin' => 'required|string|max:10',
            'representante' => 'required|string|max:255',
            'lugar' => 'sometimes|string|max:255',
            'tematica' => 'sometimes|string|max:255'
        ];
    }
}
