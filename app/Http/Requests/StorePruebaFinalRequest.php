<?php

namespace App\Http\Requests;

use App\Models\PruebaFinal;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class StorePruebaFinalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        return $user && $user->tokenCan('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nota_teorica' => 'required|numeric',
            'nota_experimental' => 'required|numeric',
            'jornada_id' => 'required|numeric|exists:jornadas,id',
            'unidad_educativa_id' => 'required|numeric|exists:unidad_educativas,id',
            'obtuvo_medalla' => 'required|boolean',
            'medalla' => [
                'required_if:obtuvo_medalla,1',
                'nullable',
                Rule::in(PruebaFinal::MEDALLAS)
            ]
        ];
    }
}
