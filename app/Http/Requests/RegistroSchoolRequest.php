<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistroSchoolRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'unidad_educativa' => 'required|string|max:255',
            'password' => 'required|string|confirmed'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
