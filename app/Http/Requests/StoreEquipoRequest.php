<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreEquipoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        return Auth::check() && ($user->tokenCan('school') || $user->tokenCan('admin'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'jornada' => 'required|numeric|exists:jornadas,id',
            'nombre_docente' => 'required|string|max:255',
            'apellido_docente' => 'required|string|max:255',
            'email_docente' => 'required|email|max:255|unique:docentes,email',
            'asignatura_docente' => 'required|string|max:255',
            'asignatura_adicional_docente' => 'sometimes|string|max:255',
            'telefono_docente' => 'sometimes|string|max::255',
            'estudiante' => 'required|array|size:5',
            'estudiante.*.nombres' => 'required|string|max:255',
            'estudiante.*.apellidos' => 'required|string|max:255',
            'estudiante.*.email' => 'required|email|max:255',
            'estudiante.*.cedula' => 'required|numeric|unique:estudiantes,cedula',
            'estudiante.*.telefono' => 'required|string|max:255',
            'estudiante.*.sexo' => 'required|in:F,M',
            'estudiante.*.nacimiento' => 'required|date|before:today',
            'estudiante.*.anio_curso' => 'sometimes|numeric|max:255'

        ];
    }
}
