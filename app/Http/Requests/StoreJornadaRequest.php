<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreJornadaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        return $user && $user->tokenCan('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:255|unique:jornadas',
            'anio_inicio' => 'required|string|max:10',
            'anio_fin' => 'required|string|max:10',
            'representante' => 'required|string|max:255',
            'lugar' => 'sometimes|string|max:255',
            'tematica' => 'sometimes|string|max:255'
        ];
    }
}
