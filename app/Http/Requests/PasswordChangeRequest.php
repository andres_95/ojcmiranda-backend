<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PasswordChangeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'token' => 'required|string',
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:8|confirmed'
        ];
    }

    public function authorize(): bool
    {
        return Auth::guest();
    }
}
