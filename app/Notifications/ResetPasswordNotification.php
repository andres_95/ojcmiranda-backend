<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class ResetPasswordNotification extends Notification
{
    /**
     * @var string
     */
    protected string $token;

    /**
     * @var User
     */
    protected User $user;

    public function __construct(User $user, string $token)
    {
        $this->token = $token;
        $this->user = $user;
    }

    public function via($notifiable): array
    {
        return ['mail'];
    }

    public function toMail($notifiable): MailMessage
    {
        $url = env('WEB_URL', 'http://localhost:8080') . '/auth/new-password?token=' . $this->token;
        $name = $this->user->nombres . ' ' . $this->user->apellidos;
        return (new MailMessage)
            ->subject('[Olimpiada Juvenil de ciencias] Recuperaciòn de contraseña')
            ->line('Usted está recibiendo este correo electrónico porque hemos recibido una solicitud de restablecimiento de contraseña para su cuenta.')
            ->line('Ingrese el siguiente codigo en la aplicación para poder recuperar su contraseña')
            ->line(new HtmlString('<h1 style="text-align: center">' . $this->token . '</h1>'))
            ->line('Si no utiliza este enlace en un plazo de 3 horas, caducará.')
            ->line('Si no ha solicitado un restablecimiento de contraseña, no es necesario realizar ninguna otra acción.')
            ->greeting(new HtmlString('Hola <br>' . $name));

    }

    public function toArray($notifiable): array
    {
        return [];
    }
}
